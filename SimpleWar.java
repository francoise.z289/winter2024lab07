public class SimpleWar{
	public static void main(String[]args){
		
		
		//Game setting
		Deck SWGame=new Deck();
		SWGame.shuffle();
		int p1Score=0;
		int p2Score=0;
		double p1Value=0;
		double p2Value=0;
		int round=1;
		Card p1=new Card("default",1);
		Card p2=new Card("default",1);
		//Game Start
		while(SWGame.length()>0){
			System.out.println("Round: "+round);
			
			p1=SWGame.drawTopCard();
			p2=SWGame.drawTopCard();
		
			System.out.println("p1: "+p1+"    p2: "+p2);
		
			p1Value=p1.calculateScore();
			p2Value=p2.calculateScore();
		
			if(p1Value>p2Value){
			System.out.println("P1 win round "+round);
			p1Score++;
			}else{
			System.out.println("P2 win round "+round);
			p2Score++;
			}
			
			System.out.println("p1Score: "+p1Score+"    p1Score: "+p2Score);
			round++;
		}
		
		//decision
		if(p1Score>p2Score){
			System.out.println("P1 win the Whole game!");
		}else{
			System.out.println("P2 win the Whole game!");
		}
		
		
	}
}