public class Card{
	private String suit;
	private int value;
	
	//constructor
	public Card(String suit,int value){
		this.suit=suit;
		this.value=value;
	}
	
	//getter
	public String getSuit(){
		return this.suit;
	}
	public int getValue(){
		return this.value;
	}
	
	public String toString(){
		return this.getValue()+" of "+this.getSuit();
	}
	
	//custom method
	public double calculateScore(){
		//convert value to double
		double suit=this.convertSuit();
		double result=suit+this.value;
		return result;
	}
	

	private double convertSuit(){
		//convert suit to number
		double result=0;
		if(this.suit.equals("Heart")){
			result=0.4;
		}
		if(this.suit.equals("Diamond")){
			result=0.3;
		}
		if(this.suit.equals("Club")){
			result=0.2;
		}
		if(this.suit.equals("Space")){
			result=0.1;
		}
		return result;
	}
	
	
}